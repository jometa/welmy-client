export function normalize_words (w) {
  if (!w) return ''
  const tokens = w.split('_')
  return tokens.map(tok => tok.toLowerCase()).join(' ')
}