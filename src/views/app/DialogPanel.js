import React, { useContext } from 'react'
import { StoreContext } from 'welm/views/app/Store'
import Sad from './sad.svg'

export default function DialogPanel () {
  const [state, dispatch] = useContext(StoreContext)
  return (
    <div 
      style={{ 
        position: "fixed",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        zIndex: 22,
        background: 'rgba(245, 245, 245, 0.8)',
        display: state.warning.show ? 'block' : 'none'
      }}
    >
      <div className="p-12 shadow bg-white mw-">
        <img src={Sad} height="64" width="64" />
        <p className="font-bold text-xl mb-4">{state.warning.message}</p>
        <div className="flex flex-row justify-start items-center font-semibold">
          <button 
            className="appearance-none bg-red-800 p-2 rounded text-white mr-2"
            onClick={() => {
              state.warning.onContinue()
              dispatch({ type: 'HIDE_WARNING' })
            }}
          >
            Lanjutkan
          </button>
          <button 
            className="appearance-none border border-gray-300 rounded p-2"
            onClick={state.warning.onCancel}
          >
            Batal
          </button>
        </div>
      </div>
    </div>
  );
}