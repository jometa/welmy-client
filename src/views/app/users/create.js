import React from 'react';
import { useMutation } from '@apollo/client';
import { useForm } from "react-hook-form"
import { useHistory } from 'react-router-dom';
import CreateUser from 'welm/gql/CreateUser'

export default function UserCreate () {
  const history = useHistory();
  const { register, handleSubmit, watch, errors } = useForm();
  let [ createUser ] = useMutation(CreateUser, {
    onCompleted: data => {
      history.goBack()
    }
  });
  
  return (
    <div>
      <div className="border-b border-gray-300 py-3 px-8 bg-gray-200 flex flex-row justify-between">
        <div className="font-bold text-2xl">Input Data User</div>
        <div>
          <button
            form="create-user"
            type="submit"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
            Simpan
          </button>
        </div>
      </div>

      <form 
        id="create-user" 
        className="w-1/3 my-4 mx-auto p-4 shadow"
        onSubmit={handleSubmit(payload => {
          createUser({
            variables: {
              input: {
                user: payload
              }
            }
          })
        })}
      >
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2">
            Username
          </label>
          <input 
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            type="text" 
            name="username"
            placeholder="Username" 
            ref={register({ required: true })}
          />
          <div className="text-red-600 text-xs font-semibold">{ errors.username && 'username harus diisi'}</div>
        </div>

        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2">
            Password
          </label>
          <input 
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            type="password" 
            name="password"
            placeholder="Password" 
            ref={register({ required: true })}
          />
          <div className="text-red-600 text-xs font-semibold">{ errors.password && 'password harus diisi'}</div>
        </div>

      </form>
    </div>
  );
}