import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { useQuery, useMutation } from '@apollo/client'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Loader from 'welm/components/Loader'
import AllUsers from 'welm/gql/AllUsers'
import DeleteUserByUsername from 'welm/gql/DeleteUserByUsername'

export default function UserList () {
  const [ keyword, setKeyword ] = useState('');
  const { loading, error, data, refetch } = useQuery(AllUsers, {
    variables: {
      keyword: `%${keyword}%`
    },
    fetchPolicy: 'network-only'
  });
  const [ deleteUser ] = useMutation(DeleteUserByUsername, {
    onCompleted: (data) => {
      refetch()
    }
  });

  if (loading) {
    return (
      <div className="w-full h-full flex items-center justify-center">
        <Loader size={128} />
      </div>
    )
  }

  else if (error) {
    console.log(error)
    return (<div>error: {error}</div>)
  }

  return (
    <div>
      <div className="border-b border-gray-400 py-4 px-8 flex flex-row justify-between bg-gray-200">
        <div>
          <div className="font-bold text-2xl">Data Users</div>
          <div className="font-medium text-sm">
            total: 
            { 
              (!error && !loading && data.allUsers.totalCount)
              ? data.allUsers.totalCount
              : ''
            }
          </div>
        </div>
        <div className="flex flex-row justify-end items-center">
          <input 
            className="appearance-none border rounded py-1 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mr-2"
            type="text" 
            placeholder="keyword username..." 
            value={keyword}
            onChange={ev => {
              const val = ev.target.value
              setKeyword(val)
            }}
          />
          <Link 
            to="/app/users/create"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-4 rounded">
            Tambah
          </Link>
        </div>
      </div>

      <div className="px-8 my-10 h-full w-2/3">
        <table className="w-full border-collapse">
          <thead>
            <tr>
              <th className="text-left border-b-2 border-gray-300 py-4 px-2 text-gray-800">
                username
              </th>
              <th className="text-right border-b-2 border-gray-300 py-4 px-2 text-gray-800">
              </th>
            </tr>
          </thead>
          <tbody>
            { data.allUsers.nodes.map(row => (
              <tr key={row.id} className="hover:bg-gray-200 text-sm">
                <td className="text-left border-b border-gray-200 py-3 px-2 text-gray-800">
                  { row.username }
                </td>
                <td className="text-left border-b border-gray-200 py-3 px-2 text-gray-800">
                  <button 
                    className="appearance-none text-red-400"
                    onClick={() => deleteUser({ variables: { username: row.username } })}
                  >
                    <FontAwesomeIcon icon='trash' />
                  </button>
                </td>
              </tr>
            )) }
          </tbody>
        </table>
      </div>
    </div>
  );
}