import React from 'react'
import { Switch, Link, Route } from 'react-router-dom'

import LahanList from './lahan/list'
import LahanCreate from './lahan/create'
import LahanUpdate from './lahan/update'
import TestInput from './testing/test.input'
import Cv from './cv/index'
import UserList from './users/list'
import UserCreate from './users/create'

export default function Routes () {
  return (
    <Switch>
      <Route exact path="/app/lahan">
        <LahanList  />
      </Route>
      <Route exact path="/app/lahan/create">
        <LahanCreate  />
      </Route>
      <Route path="/app/lahan/:id/update">
        <LahanUpdate />
      </Route>
      <Route exact path="/app/testing/input">
        <TestInput  />
      </Route>
      <Route exact path="/app/cv">
        <Cv />
      </Route>
      <Route exact path="/app/users">
        <UserList />
      </Route>
      <Route exact path="/app/users/create">
        <UserCreate />
      </Route>
    </Switch>
  );
}