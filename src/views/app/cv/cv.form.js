import React from 'react'
import { useForm } from "react-hook-form"

export default function CvForm ({ id, submit }) {
  const { register, handleSubmit, watch, errors } = useForm();

  return (
    <form id={id} onSubmit={handleSubmit(submit)}>
      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">
          Jumlah Fold
        </label>
        <input 
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          type="number" 
          name="nfold"
          min="2"
          max="10"
          placeholder="Jumlah Fold" 
          ref={register({ required: true })}
        />
        <div className="text-red-600 text-xs font-semibold">{ errors.nfold && 'jumlah fold harus diisi'}</div>
      </div>
    </form>
  );
}
