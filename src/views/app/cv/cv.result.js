import React from 'react'

export default function CvResult ({ items }) {
 return (
   <table className="w-full border-collapse">
     <thead>
       <tr>
         <th className="text-left border-b border-gray-200 py-3 px-2 text-gray-800"></th>
         <th className="text-left border-b border-gray-200 py-3 px-2 text-gray-800">total data</th>
         <th className="text-left border-b border-gray-200 py-3 px-2 text-gray-800">mean akurasi</th>
         <th className="text-left border-b border-gray-200 py-3 px-2 text-gray-800">%</th>
       </tr>
     </thead>
     <tbody>
       {
         items && items.map((item, index) => {
           return (
            <tr key={`part-${index}`}>
              <td className="text-left border-b border-gray-200 py-3 px-2 text-gray-800">{index + 1}</td>
              <td className="text-left border-b border-gray-200 py-3 px-2 text-gray-800">{item.totalData}</td>
              <td className="text-left border-b border-gray-200 py-3 px-2 text-gray-800">{item.meanAccuracy.toFixed(4)}</td>
              <td className="text-left border-b border-gray-200 py-3 px-2 text-gray-800">{(item.meanAccuracy * 100).toFixed(4)} %</td>
            </tr>
           )
         })
       }
     </tbody>
   </table>
 ) 
}