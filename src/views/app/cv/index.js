import React, { useState, useEffect } from 'react'
import { useQuery } from '@apollo/client'
import { spawn, Thread, Worker } from "threads"
import AllLahansFull from 'welm/gql/AllLahansFull'
import CvForm from './cv.form'
import CvResult from './cv.result'
import Loader from 'welm/components/Loader'
import ErrorPane from 'welm/components/ErrorPane'


export default function Cv () {
  const { loading, error, data } = useQuery(AllLahansFull, {
    fetchPolicy: 'network-only'
  })

  const [ cvResult, setCvResult ] = useState(null)
  const [ worker, setWorker ] = useState(null)
  useEffect(() => {
    spawn(new Worker('../../../services/cv_worker.js'))
      .then(_worker => {
        setWorker(_worker)
      })
    return function cleanUpWorker () {
      if (worker) {
        Thread.terminate(worker)
      }
    }
  }, [])

  if (loading) {
    return (
      <div className="w-full h-full flex items-center justify-center">
        <Loader size={128} />
      </div>
    )
  }
  if (error) {
    return (
      <ErrorPane />
    )
  }
  
  return (
    <div>
      <div className="border-b border-gray-300 py-3 px-8 bg-gray-200 flex flex-row justify-between">
        <div className="font-bold text-2xl">Cross Validation</div>
        <div>
          {
            cvResult == null
            ?
              <button
                form="cv-form"
                type="submit"
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                Jalankan
              </button>
            :
              <button
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                onClick={() => setCvResult(null)}
              >
                Ulangi
              </button>
          }
        </div>
      </div>

      <div className="px-8">
        {
          cvResult == null
          ?
            <div className="w-1/3 my-12 mx-auto">
              <CvForm 
                id='cv-form' 
                submit={payload => {
                  worker.cross_validation({ nfold: payload.nfold, items: data.allLahans.nodes })
                    .then(items => {
                      let cvResult = items.map(item => {
                        return {
                          totalData: item.n,
                          meanAccuracy: item.hit_ratio
                        }
                      })
                      setCvResult(cvResult)
                    })
                }}
              >
              </CvForm>
            </div>
          :
            <div className="my-12">
              <CvResult items={cvResult} />
            </div>
        }
      </div>
    </div>
  )
}