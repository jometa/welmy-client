import React from 'react';
import { useMutation, useQuery } from '@apollo/client';
import { useHistory, useParams } from 'react-router-dom';
import LahanForm from './lahan.form';
import LahanById from 'welm/gql/LahanById'
import UpdateLahanById from 'welm/gql/UpdateLahanById'
import Loader from 'welm/components/Loader';

export default function LahanList () {
  let history = useHistory();
  const { id } = useParams();
  
  const { data, loading: queryLoading, error: queryError } = useQuery(LahanById, {
    variables: {
      id: parseInt(id)
    },
    fetchPolicy: 'network-only'
  });

  let [ updateLahanById, { data: mutData, error: mutError, loading: mutLoading } ] = useMutation(UpdateLahanById, {
    onCompleted: data => {
      history.goBack()
    }
  });


  if (queryLoading || mutLoading) {
    return (
      <div className="w-screen h-full flex items-center justify-center">
        <Loader size={128} />
      </div>
    );
  }

  else if (queryError) {
    return (<div>Gagal mengambil data lahan</div>);
  }

  else if (mutError) {
    return (<div>Gagal mengubah data lahan</div>);
  }

  return (
    <div>
      <div className="border-b border-gray-300 py-3 px-8 bg-gray-200 flex flex-row justify-between">
        <div className="font-bold text-2xl">Edit Data Lahan</div>
        <div>
          <button
            form="create-lahan"
            type="submit"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
            Simpan
          </button>
        </div>
      </div>
      <div className="w-1/3 my-4 mx-auto p-4 shadow">
        <LahanForm 
          id='create-lahan'
          submit={data => {
            updateLahanById({
              variables: {
                input: {
                  id: parseInt(id),
                  lahanPatch: {
                    ...data,
                    curahHujan: parseFloat(data.curahHujan),
                    dalam: parseFloat(data.dalam)
                  }
                }
              }
            })
          }}
          defaultValues={data.lahanById}
        ></LahanForm>
        <button
          form="create-lahan"
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-8">
          Simpan
        </button>
      </div>
    
    </div>
  );
}