import React from 'react';
import { useMutation } from '@apollo/client';
import { useHistory } from 'react-router-dom';
import LahanForm from './lahan.form';
import CreateLahan from 'welm/gql/CreateLahan'

export default function LahanList () {
  let history = useHistory();
  let [ createLahan, { data } ] = useMutation(CreateLahan, {
    onCompleted: data => {
      history.goBack()
    }
  });
  return (
    <div>
      <div className="border-b border-gray-300 py-3 px-8 bg-gray-200 flex flex-row justify-between">
        <div className="font-bold text-2xl">Input Data Lahan</div>
        <div>
          <button
            form="create-lahan"
            type="submit"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
            Simpan
          </button>
        </div>
      </div>

      <div className="w-1/3 my-4 mx-auto p-4 shadow">
        <LahanForm 
          id='create-lahan' 
          submit={data => {
            createLahan({
              variables: {
                input: {
                  lahan: {
                    ...data,
                    curahHujan: parseFloat(data.curahHujan),
                    dalam: parseFloat(data.dalam)
                  }
                }
              }
            })
          }}
        ></LahanForm>
        <button
          form="create-lahan"
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-8">
          Simpan
        </button>
      </div>
    </div>
  );
}