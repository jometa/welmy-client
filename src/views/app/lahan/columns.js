import { normalize_words } from 'welm/commons'

export default [
  { field: 'id' },
  { field: 'lokasi' },
  { field: 'curahHujan', 
    label: 'curah hujan'
  },
  { field: 'dalam', 
    label: 'kedalaman'
  },
  { field: 'drainase',
    format: normalize_words
  },
  { field: 'teksturTanah',
    label: 'tekstur',
    format: normalize_words },
  { label: 'c-organik', 
    field: 'corganik', 
    format: normalize_words },
  { field: 'bencana', format: normalize_words },
  {field: 'kemiringan', format: normalize_words },
  { field: 'kelas', format: normalize_words },
  { label: '' }
]