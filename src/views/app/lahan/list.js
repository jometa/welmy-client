import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { useQuery } from '@apollo/client'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import AllLahans from 'welm/gql/AllLahans'
import columns from './columns'
import DeleteButton from './DeleteButton'
import Loader from 'welm/components/Loader'

export default function LahanList () {
  let [filterKelas, setFilterKelas] = useState('');
  let [filterKeyword, setFilterKeyword] = useState("");

  let filter = {
    lokasi: { likeInsensitive: `%${filterKeyword}%` }
  };
  if (filterKelas) {
    filter.kelas = {
      equalTo: filterKelas
    }
  }

  const { loading, error, data, refetch } = useQuery(AllLahans, {
    variables: { filter },
    fetchPolicy: 'network-only'
  })

  let content;

  if (loading) {
    content = (
      <div className="w-full h-full flex items-center justify-center">
        <Loader size={128} />
      </div>
    )
  }

  else if (error) {
    console.log(error)
    content = (<div>error: {error}</div>)
  }

  else {
    content = (
      <table className="w-full border-collapse">
        <thead>
           <tr>
             { columns.map((item, index) => (
               <th 
                key={index}
                className="text-left border-b-2 border-gray-300 py-4 px-2 text-gray-800">{
                 item.label ? 
                  item.label
                  : (item.field ? item.field : '')
               }</th>
             )) }
            </tr>
        </thead>
        <tbody>
          { data.allLahans.nodes.map(row => (
            <tr key={row.id} className="hover:bg-gray-200 text-sm">
              { 
                columns.slice(0, columns.length - 1).map((item, index) => (
                 <td
                  key={`tr-${row.id}-${index}`}
                  className="text-left border-b border-gray-200 py-3 px-2 text-gray-800">{
                    item.format 
                      ? item.format(row[item.field])
                      : (
                        item.field
                        ? row[item.field]
                        : ''
                      )
                 }</td>
               )) 
              }
              <td className="text-left border-b border-gray-200 py-3 px-2 text-gray-800">
                <Link to={`/app/lahan/${row.id}/update`} className="mr-4">
                  <FontAwesomeIcon icon='pencil-alt' className="text-blue-700" />
                </Link>
                <DeleteButton 
                  id={row.id} 
                  onCompleted={refetch}
                />
              </td>
            </tr>
          )) }
        </tbody>
      </table>
    );
  }

  return (
    <div>
      {/* Top Nav */}
      <div className="border-b border-gray-400 py-4 px-8 flex flex-row justify-between bg-gray-200">
        <div>
          <div className="font-bold text-2xl">Data Lahan
          </div>
          <div className="font-medium text-sm">
            total: 
            { 
              (!error && !loading && data.allLahans.totalCount)
              ? data.allLahans.totalCount
              : ''
            }
          </div>
        </div>
      
        <div className="flex flex-row justify-end items-center">
          <div className="inline-block relative w-64 mr-2">
            <select
              value={filterKelas}
              onChange={ev => {
                if (ev != '')
                  setFilterKelas(ev.target.value)
              }}
              className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-1 pr-8 rounded leading-tight focus:outline-none focus:shadow-outline">
              <option value=''>semua</option>
              <option value="SANGAT_SESUAI">kelas: sangat sesuai</option>
              <option value="SESUAI">kelas: sesuai</option>
              <option value="CUKUP_SESUAI">kelas: cukup sesuai</option>
            </select>
            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
              <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
            </div>
          </div>
          <input 
            className="appearance-none border rounded py-1 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline mr-2"
            type="text" 
            placeholder="keyword lokasi..." 
            value={filterKeyword}
            onInput={ev => {
              const val = ev.target.value
              setFilterKeyword(val)
            }}
          />
          <Link 
            to="/app/lahan/create"
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-4 rounded">
            Tambah
          </Link>
        </div>
      </div>
    
      {/* Content */}
      <div className="px-8 my-10 h-full">
        {content}
      </div>
    </div>
  );
}