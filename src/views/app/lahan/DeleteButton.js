import React, { useContext } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useMutation } from '@apollo/client'
import { StoreContext } from 'welm/views/app/Store'
import DeleteLahanById from 'welm/gql/DeleteLahanById'

export default function DeleteLahanButton ({ id, onCompleted }) {
  const [ deleteLahanById ] = useMutation(DeleteLahanById, {
    onCompleted,
    variables: {
      input: {
        id
      }
    }
  });

  const [_, dispatch] = useContext(StoreContext)

  return (
    <button 
      className="appearance-none text-red-400"
      onClick={() => {
        dispatch({
          type: 'SHOW_WARNING',
          payload: {
            message: 'Menghapus data lahan akan mempengaruhi hasil uji coba dan cross validation.',
            onContinue: deleteLahanById,
            onCancel: () => dispatch({
              type: 'HIDE_WARNING'
            })
          }
        })
      }}
    >
      <FontAwesomeIcon icon='trash' />
    </button>
  );
}