import React, { useReducer } from 'react'

const initial = {
  warning: {
    show: false,
    message: '',
    onContinue: null,
    onCancel: null
  }
}

const reducer = (state, action) => {
  switch (action.type) {
    case 'SHOW_WARNING':
      return {
        ...state,
        warning:{
          ...action.payload,
          show: true
        }
      }
    case 'HIDE_WARNING':
      return {
        ...state,
        warning:{
          ...state.warning,
          show: false
        }
      }
    default:
      return state
  }
}

export const StoreContext = React.createContext(initial)

export function Store ({ children }) {
  const [state, dispatch] = useReducer(reducer, initial);
  return (
    <StoreContext.Provider value={[state, dispatch]}>
      {children}
    </StoreContext.Provider>
  );
}
