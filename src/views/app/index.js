import React from 'react'
import { useHistory } from 'react-router-dom'
import { Switch, Link, Route } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './index.css'
import Logo from './healthy-food.svg'
import Routes from './Routes'
import { Store } from './Store'
import DialogPanel from './DialogPanel'

const sideMenus = [
  { label: 'data lahan', path: '/app/lahan', icon: 'database' },
  { label: 'uji coba', path: '/app/testing/input', icon: 'vials' },
  { label: 'cross validation', path: '/app/cv', icon: 'tasks' },
  { label: 'users', path: '/app/users', icon: 'users' }
]

export default function AppWrapper () {
  return (
    <div className="app-wrapper">
      <section className="wrapper">

        {/* Top nav */}
        <nav 
          className="topnav flex flex-col"
        >
          <div className="flex flex-row justify-between px-8 bg-black text-white flex-grow">
            <div className="flex flex-row items-center">
              <img className="mx-auto mr-4 text-white" width="36" height="36" src={Logo} />
              <div className="text-xl uppercase fredoka">CBR / Welm</div>
            </div>
            <div className="flex flex-row items-center">
              <Link className="my-2 font-semibold px-3 mr-2" to='/app/about'>about</Link>
              <Link className="my-2 font-semibold px-3" to='/app/logout'>logout</Link> 
            </div>
          </div>
          <div className="splash h-8"></div>
        </nav>

        <nav className="flex flex-row px-8 border-b border-gray-400">
        { sideMenus.map(item => (
            <Link 
              className="py-3 flex flex-row items-center hover:bg-red-800 mr-10"
              to={item.path} key={item.path}>
              <div className="flex items-center w-6 text-center">
                <FontAwesomeIcon icon={item.icon} />
              </div>
              <span className="ml-2">{item.label}</span>
            </Link>
          )) }
        </nav>

        <div className="content">
          <Store>
            <React.Fragment>
              <Routes />
              <DialogPanel />
            </React.Fragment>
          </Store>
        </div>

      </section>
      <footer className="footer flex flex-row justify-between px-8 border-t border-gray-400 text-gray-700">
        <div className="flex flex-row items-center">
          <div className="font-fredoka">copyright welmy 2020</div>
        </div>
        <div className="flex flex-row items-center">
          <Link className="my-2 px-3 mr-2" to='/app/about'>about</Link>
          <Link className="my-2 px-3" to='/app/logout'>logout</Link>
        </div>
      </footer>
    </div>);
}