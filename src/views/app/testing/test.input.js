import React, { useEffect, useState } from 'react';
import { gql } from '@apollo/client'
import { useMutation, useQuery } from '@apollo/client';
import { useHistory } from 'react-router-dom';
import { spawn, Thread, Worker } from "threads"
import TestForm from './test.form';
import TestResult from './test.result'
import Loader from 'welm/components/Loader'

const AllLahans = gql`
  query AllLahans {
    allLahans {
      nodes {
        bencana
        corganik
        curahHujan
        drainase
        id
        kelas
        kemiringan
        dalam
        nodeId
        lokasi
        teksturTanah
      }
    }
  }
`;

export default function TestInput () {
  const { loading, error, data } = useQuery(AllLahans, {
    fetchPolicy: 'network-only'
  });

  const [ testResult, setTestResult ] = useState(null);
  const [ worker, setWorker ] = useState(null);
  useEffect(() => {
    spawn(new Worker('../../../services/cbr_worker.js'))
      .then(_worker => {
        setWorker(_worker)
      })
    return function cleanUpWorker () {
      if (worker) {
        Thread.terminate(worker)
      }
    }
  }, [])

  if (loading) {
    return (
      <div className="w-full h-full flex items-center justify-center">
        <Loader size={128} />
      </div>
    )
  }

  else if (error) {
    console.log(error)
    return (<div>error: {error}</div>)
  }

  return (
    <div>
      <div className="border-b border-gray-300 py-3 px-8 bg-gray-200 flex flex-row justify-between">
        <div className="font-bold text-2xl">Uji Coba</div>
        <div>
          {
            testResult == null
            ?
              (<button
                form="test-form"
                type="submit"
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                Jalankan
              </button>)
            :
              (<button
                form="test-form"
                onClick={() => setTestResult(null)}
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                Ulangi
              </button>)
          }
        </div>
      </div>

      {
        testResult == null
        ? 
          <div className="w-1/3 my-4 mx-auto p-4 shadow">
            <TestForm
              id='test-form'
              submit={input => {
                if (!worker) {
                  return
                }
                worker.retrieve({ input, items: data.allLahans.nodes })
                  .then(result => {
                    console.log(result)
                    const testResult = {
                      ...result,
                      input
                    }
                    setTestResult(testResult)
                  })
              }}
            >
            </TestForm>
            <button
              form="test-form"
              type="submit"
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-8">
              Jalankan
            </button>
          </div>
        : 
          <div className="my-4 px-8">
            <TestResult {...testResult} />
          </div>
    }
    </div>
  );
}