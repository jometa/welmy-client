import React from 'react'
import { useForm } from "react-hook-form"

export default function TestForm (options) {
  const { submit, id } = options;
  const defaultValues = options.defaultValues ? options.defaultValues : {};
  const { register, handleSubmit, watch, errors } = useForm({ defaultValues });

  return (
    <form id={id} onSubmit={handleSubmit(submit)}>

      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">
          Lokasi
        </label>
        <input 
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          type="text" 
          name="lokasi"
          placeholder="Lokasi Lahan" 
          ref={register({ required: true })}
        />
        <div className="text-red-600 text-xs font-semibold">{ errors.lokasi && 'lokasi lahan harus diisi'}</div>
      </div>

      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">
          Curah Hujan
        </label>
        <input 
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          type="number" 
          min="0"
          name="curahHujan"
          placeholder="Curah Hujan" 
          ref={register({ required: true })}
        />
        <div className="text-red-600 text-xs font-semibold">{ errors.curahHujan && 'curah hujan harus diisi'}</div>
      </div>

      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">
          Kedalaman Tanah
        </label>
        <input 
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
          type="number" 
          min="0"
          name="dalam"
          placeholder="Kedalaman Tanah" 
          ref={register({ required: true })}
        />
        <div className="text-red-600 text-xs font-semibold">{ errors.dalam && 'kedalaman tanah harus diisi'}</div>
      </div>

      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">
          Drainase
        </label>
        <div className="inline-block relative w-64">
          <select 
            name="drainase"
            ref={register({ required: true })}
            className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
            <option value="BAIK">baik</option>
            <option value="AGAK_TERLAMBAT">agak terhambat</option>
            <option value="AGAK_CEPAT">agak cepat</option>
            <option value="TERHAMBAT">terhambat</option>
          </select>
          <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
          </div>
        </div>
      </div>

      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">
          Tekstur Tanah
        </label>
        <div className="inline-block relative w-64">
          <select 
            name="teksturTanah"
            ref={register({ required: true })}
            className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
            <option value="HALUS">halus</option>
            <option value="AGAK_HALUS">agak halus</option>
            <option value="SEDANG">sedang</option>
            <option value="AGAK_KASAR">agak kasar</option>
          </select>
          <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
          </div>
        </div>
      </div>

      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">
          C-Organik
        </label>
        <div className="inline-block relative w-64">
          <select
            name="corganik"
            ref={register({ required: true })}
            className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
            <option value="TERSEDIA">tersedia</option>
            <option value="CUKUP_TERSEDIA">cukup tersedia</option>
            <option value="KURANG_TERSEDIA">kurang tersedia</option>
          </select>
          <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
          </div>
        </div>
      </div>

      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">
          Bencana
        </label>
        <div className="inline-block relative w-64">
          <select 
            name="bencana"
            ref={register({ required: true })}
            className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
            <option value="TIDAK_ADA">tidak ada</option>
            <option value="RINGAN">ringan</option>
            <option value="SEDANG">sedang</option>
            <option value="BERAT">berat</option>
          </select>
          <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
          </div>
        </div>
      </div>

      <div className="mb-4">
        <label className="block text-gray-700 text-sm font-bold mb-2">
          Kemiringan Tanah
        </label>
        <div className="inline-block relative w-64">
          <select 
            name="kemiringan"
            ref={register({ required: true })}
            className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
            <option value="DATAR">datar</option>
            <option value="AGAK_BERBUKIT">agak berbukit</option>
            <option value="BERBUKIT">berbukit</option>
          </select>
          <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
          </div>
        </div>
      </div>
    </form>
  );
}