import React from 'react';
import TestAltView from './test.alt.view'
import { normalize_words } from 'welm/commons'

export default function TestResult ({
  closest,
  indexing,
  sim,
  kelas,
  input
}) {
  return (
    <div className="grid grid-cols-3 gap-8">
      <div>
        <div className="text-xl mb-4">Hasil Indexing</div>
        <div className="text-xl font-semibold my-4">{normalize_words(kelas)}</div>
        <div className="text-xl font-semibold my-4">Probabilitas Kelas: {indexing.prob}</div>
        <div className="text-xl font-semibold my-4">Tingkat Kemiripan: {(sim / 7.0).toFixed(2)}</div>
      </div>
      <div>
        <div className="text-xl mb-4">Input</div>
        <TestAltView item={input} />
      </div>
      <div>
        <div className="text-xl mb-4">Data Termirip</div>
        <TestAltView item={closest} />
      </div>
    </div>
  );
}