import React from 'react'
import { useHistory } from 'react-router-dom'

export default function AuthWrapper ({ children }) {
  const history = useHistory()
  const username = localStorage.getItem('welm.username')
  if (!username || username == '') {
    history.replace('/auth')
  }
  return children
}