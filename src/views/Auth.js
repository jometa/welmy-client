import React, { useState } from 'react'
import { useQuery } from '@apollo/client'
import { Link, useHistory } from 'react-router-dom'
import UserByUsername from 'welm/gql/UserByUsername'
import './app/index.css'
import Logo from './app/healthy-food.svg'
import Loader from 'welm/components/Loader'
import ErrorPane from 'welm/components/ErrorPane'
import { client } from 'welm/apollo'

export default function Auth () {
  const history = useHistory();
  const [ username, setUsername ] = useState('');
  const [ password, setPassword ] = useState('');
  const [ errorMessage, setErrorMessage ] = useState('');

  const onLogin = () => {
    if (username == '' || password == '') {
      setErrorMessage('Invalid auth')
      return
    }
    client.query({
      query: UserByUsername,
      variables: {
        username: username
      }
    }).then(resp => {
      if (resp.data && resp.data.userByUsername) {
        const user = resp.data.userByUsername
        if (user.password == password) {
          localStorage.set('welm.username', username)
          history.push('/app/lahan')
        }
      } else {
        setErrorMessage('Terjadi Kesalahan')
      }
    })
  }

  return (
    <div className="app-wrapper">
      <section className="wrapper flex flex-col">
        <nav 
          className="topnav flex flex-col"
        >
          <div className="flex flex-row justify-between px-8 bg-black text-white flex-grow">
            <div className="flex flex-row items-center">
              <img className="mx-auto mr-4 text-white" width="36" height="36" src={Logo} />
              <div className="text-xl uppercase fredoka">CBR / Welm</div>
            </div>
            <div className="flex flex-row items-center">
              <Link className="my-2 font-semibold px-3 mr-2" to='/app/about'>about</Link>
              <Link className="my-2 font-semibold px-3" to='/app/logout'>logout</Link> 
            </div>
          </div>
          <div className="splash h-8"></div>
        </nav>
        <div className="flex-grow bg-gray-900 flex flex-col justify-center items-center text-white">
          <img width="128" height="128" src={Logo} />
          {
            errorMessage != '' &&
            <div className="text-gray-300 font-bold text-2xl text-center">
              {errorMessage}
            </div>
          }
          <div className="mw-64">
            <div className="mb-6 flex flex-col">
              <label className="text-lg font-bold">Username</label>
              <input 
                type="text" 
                name="username" 
                className="appearance-none border-4 border-gray-800 px-2 py-1 bg-black text-white text-lg font-semibold"
                value={username}
                onChange={event => setUsername(event.target.value)}
              ></input>
            </div>
            <div className="mb-6 flex flex-col">
              <label className="text-lg font-bold">Password</label>
              <input 
                type="password" 
                name="password" 
                className="appearance-none border-4 border-gray-800 px-2 py-1 bg-black text-white text-lg font-semibold"
                value={password}
                onChange={event => setPassword(event.target.value)}
              ></input>
            </div>
          </div>
          <button 
            onClick={onLogin}
            className="appearance-none rounded p-2 px-4 bg-orange-700 shadow-1 text-xl font-bold my-4"
          >login</button>
        </div>
      </section>

      <footer className="footer flex flex-row justify-between px-8 border-t-4 border-gray-700 text-gray-200 bg-gray-800">
        <div className="flex flex-row items-center">
          <div className="font-fredoka">copyright welmy 2020</div>
        </div>
        <div className="flex flex-row items-center">
          <Link className="my-2 px-3 mr-2" to='/app/about'>about</Link>
          <Link className="my-2 px-3" to='/app/logout'>logout</Link>
        </div>
      </footer>
    </div>
  );
}