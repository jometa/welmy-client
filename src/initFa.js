import { library } from '@fortawesome/fontawesome-svg-core'
import { 
    faCheckSquare, 
    faCoffee, 
    faTable, 
    faDatabase,
    faVials,
    faTasks,
    faUsers,
    faPencilAlt,
    faTrash
} from '@fortawesome/free-solid-svg-icons'
 
library.add(
    faCheckSquare, 
    faCoffee, 
    faTable, 
    faDatabase,
    faVials,
    faTasks,
    faUsers,
    faPencilAlt,
    faTrash
)