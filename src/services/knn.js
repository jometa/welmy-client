import { maxBy, max } from 'lodash'

export function knn({ input, xs }) {
  const closeness = xs.map(x => {
    return x.filter((xi, index) => xi == input[index]).length
  })
  let max_index = -1
  let max_sim = -1
  closeness.forEach((sim, index) => {
    if (sim > max_sim) {
      max_sim = sim
      max_index = index
    }
  })
  return { max_sim, max_index }
}