import { uniqueBy, uniq, range } from 'lodash'

export function naive_bayes (options) {
  const { input, xs, ys, smooth: _smooth } = options
  const smooth = _smooth ? _smooth : 3
  const dim = xs[0].length;
  const total = xs.length;
  let Kelas = {
    SANGAT_SESUAI: 0,
    SESUAI: 0,
    CUKUP_SESUAI: 0
  }

  ys.forEach((y, index) => {
    if (isNaN(!Kelas[y])) {
      throw new Error(`unknown y: ${y}, at index: ${index}, Kelas[y] = ${Kelas[y]}`)
    }
    Kelas[y] += 1
  })


  const classes = uniq(ys)
  const z = classes.length
  return classes
    .map(cls => {
      const xs_in_class = xs.filter((x, index) => ys[index] == cls)
      const n = xs_in_class.length
      let result = range(dim)
        .map(idim => {
          const count = xs_in_class.filter(x => x[idim] == input[idim]).length
          return (count + smooth) / (n + (smooth * z))
        })
        .reduce((a, b) => a * b, 1)
      return { cls, prob: result * (Kelas[cls] / total) }
    })
    .reduce((prev, next) => {
      if (next.prob > prev.prob) {
        return next
      }
      return prev
    }, { cls: '', prob: -1 })
}