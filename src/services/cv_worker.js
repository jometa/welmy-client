import { expose } from "threads/worker"
import { cross_validation } from './cv';

expose({
  cross_validation
})
