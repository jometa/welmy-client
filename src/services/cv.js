import { range, concat, sum } from 'lodash'
import { retrieve } from './cbr';

export function cross_validation ({ nfold, items }) {
  let parts = items.reduce((accu, current, index) => {
    let part_index = index % nfold
    accu[part_index].push(current)
    return accu
  }, range(nfold).map(i => []))

  return parts.map((test_part, index) => {
    const n = test_part.length

    const train_part = parts
      .filter((p, pindex) => pindex != index)
      .reduce((accu, current) => concat(accu, current))

    const hit = sum(
      test_part.map(input => {
        const result = retrieve({ input, items: train_part })
        if (input.kelas != result.kelas) {
          return 0
        }
        return 1
      })
    )

    return {
      n,
      hit,
      hit_ratio: 1.0 * hit / n
    }
  })
}
