import { expose } from "threads/worker"
import { retrieve } from './cbr';

expose({
  retrieve
})
