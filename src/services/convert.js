function _ch (x) {
  if (x > 400 && x <= 500) return 3;
  if (x > 500 && x <= 600) return 2;
  if (x > 600 && x <= 1200) return 1;
  if (x > 1200 && x <= 1400) return 1;
  if (x > 1400) return 3;
  throw new Error(`unknown curahHujan: ${x}`);
}

function _kt (x) {
  if (x > 30 && x <= 50) return 3;
  if (x > 50 && x <= 75) return 2;
  if (x > 75) return 1;
  throw new Error(`unknown kedalamanTanah: ${x}`)
}

export function convert (item) {
  return [
    item.bencana,
    item.corganik,
    _ch(item.curahHujan),
    _kt(item.dalam),
    item.drainase,
    item.kemiringan,
    item.teksturTanah
  ];
}