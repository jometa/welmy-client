import { convert } from './convert';
import { naive_bayes } from './naive-bayes';
import { knn } from './knn'

export function retrieve (options) {
  if (!options.input) {
    throw new Error('please provide `input`');
  }
  if (!options.items) {
    throw new Error('please provide `items`');
  }
  const input = convert(options.input);
  const _temp = options.items.map(item => {
    return { 
      xs: convert(item),
      ys: item.kelas
    }
  });
  const xs = _temp.map(it => it.xs);
  const ys = _temp.map(it => it.ys);

  const max_class = naive_bayes({ input, xs, ys });
  const subset = xs.map((x, index) => ({ index, x }));
  
  const { max_sim, max_index } = knn({ input, xs: subset.map(it => it.x) })
  const max_glob_index = subset[max_index].index
  const closest = options.items[max_glob_index]
  const indexing = max_class
  const sim = max_sim
  return {
    closest,
    indexing,
    sim,
    kelas: closest.kelas
  }
}

