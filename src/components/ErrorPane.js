import React from 'react'
import SadSvg from 'welm/assets/sad.svg'

export default function ErrorPane (opts) {
  let message = opts.message ? opts.message : 'Terjadi kesalahan';
  return (
    <div className="error-pane h-full w-full p-12 text-center">
      <img src={SadSvg} height="128" width="128" />
      <p className="font-bold text-lg text-red-900">{ message }</p>
    </div>
  );
}