import React from 'react'
import Logo from 'welm/assets/ui.svg'
import './Loader.css'

export default function Loader (opts) {
  let styles = {};
  let size = opts.size ? opts.size : 64;
  styles.width = `${size}px`;
  styles.height = `${size}px`;
  return (
    <img className="spinning" src={Logo} style={styles} />
  );
}