import { gql } from '@apollo/client'

export default gql`
  query AllUsers($keyword: String!) {
    allUsers(filter: {username: {likeInsensitive: $keyword}}) {
      nodes {
        password
        username
        nodeId
      }
      totalCount
    }
  }
`
