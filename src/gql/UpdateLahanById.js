import { gql } from '@apollo/client'

export default gql`
  mutation UpdateLahanById($input: UpdateLahanByIdInput!) {
    updateLahanById(input: $input) {
      lahan {
        id
      }
    }
  }
`
