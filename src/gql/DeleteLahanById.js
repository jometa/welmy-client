import { gql } from '@apollo/client'

export default gql`
  mutation DeleteLahanById ($input: DeleteLahanByIdInput!) {
    deleteLahanById(input: $input) {
      lahan {
        id
      }
    }
  }
`
