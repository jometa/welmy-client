import { gql } from '@apollo/client'

export default gql`
  mutation CreateLahan ($input: CreateLahanInput!) {
    createLahan(input: $input) {
      lahan {
        id
      }
    }
  }
`
