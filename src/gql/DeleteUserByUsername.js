import { gql } from '@apollo/client'

export default gql`
  mutation DeleteUserByUsername ($username: String!) {
    deleteUserByUsername(input: { username: $username }) {
      user {
        username
      }
    }
  }
`
