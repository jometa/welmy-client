import { gql } from '@apollo/client'

export default gql`
  query AllLahans {
    allLahans {
      nodes {
        bencana
        corganik
        curahHujan
        drainase
        id
        kelas
        kemiringan
        dalam
        lokasi
        nodeId
        teksturTanah
      }
    }
  }

`
