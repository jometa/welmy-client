import { gql } from '@apollo/client'

export default gql`
  query UserByUsername($username: String!) {
    userByUsername(username: $username) {
      password
      username
      nodeId
    }
  }
`
