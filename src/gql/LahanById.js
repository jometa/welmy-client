import { gql } from '@apollo/client'

export default gql`
  query LahanById ($id: Int!) {
    lahanById(id: $id) {
      bencana
      curahHujan
      corganik
      drainase
      kelas
      id
      kemiringan
      lokasi
      dalam
      nodeId
      teksturTanah
    }
  }
`