import { gql } from '@apollo/client'

export default gql`
  query AllLahans($filter: LahanFilter!) {
    allLahans(filter: $filter) {
      nodes {
        bencana
        corganik
        curahHujan
        drainase
        id
        kelas
        kemiringan
        dalam
        lokasi
        nodeId
        teksturTanah
      }
      totalCount
    }
  }

`
