// import { hot } from 'react-hot-loader/root';
import React from 'react';
import {
  HashRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import { ApolloProvider } from '@apollo/client';
import { client } from './apollo'
import Auth from './views/Auth';
import AppWrapper from './views/app';
import './styles/main.css'
import './initFa';

function App() {
  return (
    <div id="app">
      <ApolloProvider client={client}>
        <Router>
          <Switch>
            <Route path="/app">
              <AppWrapper />
            </Route>

            <Route path="/auth">
              <Auth />
            </Route>
          </Switch>
        </Router>
      </ApolloProvider>
    </div>
  );
}

export default App;
